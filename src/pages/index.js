import * as React from "react"
import { Footer } from "../components/Footer";
import "./index.css";
import { Contador } from '../components/Contador';



const IndexPage = () => {

  const contadores = [
    {
      title: "Contador 1",
      description: "Esta é a descrición do primeiro contador"
    },
    {
      title: "Contador 2",
      description: "Esta é a descrición do segundo contador"
    },
    {
      title: "Contador 3",
      description: "Esta é a descrición do terceiro contador"
    },
    {
      title: "Contador 4",
      description: "Esta é a descrición do cuarto contador lorem ipsum blablaba"
    },
  ];

  return (
    <>
    <main >
      <h1 className="main-title">
        Idoia 2023
      </h1>

      <p>
        aquí un parrafo
      </p>
     
    </main>

    {contadores.map(
      contador=> <Contador title={contador.title} description={contador.description} background={"lightgray"} />)
    }

   

    <Footer page="Portada" subfooter="texto que vai debaixo do footer"/>
    </>
  )
}

export default IndexPage

export const Head = () => <title>Home Page</title>
