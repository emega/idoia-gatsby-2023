import * as React from "react"
import { FontPreview } from "../components/FontPreview";
import { Footer } from "../components/Footer";
import "./tipografia.css";

function TipografiaPage() {

  const data = [
    {
      name: "Partitura 1941",
      description: "Aquí a descrición da tipografía Partitura lorem ipsum bla bla bla"
    },
    {
      name: "Kallaikos Reeve",
      description: "Aquí a descrición da tipografía Kallaikos lorem ipsum bla bla bla"
    },
    {
      name: "Outra",
      description: "Aquí a descrición da tipografía Outra lorem ipsum bla bla bla"
    },
    {
      name: "Outra Máis",
      description: "Aquí a descrición da tipografía Outra lorem ipsum bla bla bla"
    }
  ];


  return (
    <>
      <main >
        <h1 className="main-title">
          Tipografía
        </h1>

        {
          data.map(tipografia=><FontPreview name={tipografia.name} description={tipografia.description}/>)
        }


     


      </main>
      <Footer page="tipografía"/>
    </>
  )
}

export default TipografiaPage

export const Head = () => <title>Tipografía</title>
