import * as React from "react";
import { useState } from "react";
import "./contador.css";


export const Contador = (props) => {

    console.log("PROPS", props);

    const corDeFondo = props.background;
    const titulo = props.title;
    const texto = props.description;

    const [value, setValue] = useState(0);
    const [tamanho, setTamanho] = useState(18);

    const handleClick = () => {
        console.log("Click no botón");
        setValue(value + 1);
    }
    const handleRangeChange = (event) => {
        console.log("Cambio no rango, valor:", event.target.value);
        setTamanho(event.target.value)
    }



    return (
        <div className="counter" style={{ backgroundColor: corDeFondo }}>
            <h2>{titulo}</h2>
            <button onClick={handleClick}>Pulsar para incrementar o contador</button>
            <p>Número de clicks = {value}</p>
            <div>
                <input type="range" id="size" name="size"
                    min="10" max="25" value={tamanho} onChange={handleRangeChange} />
                <label htmlFor="size">Tamaño da letra ({tamanho})</label>
            </div>
            <p style={{fontSize: `${tamanho}px` }}>{texto}</p>

        </div>
    )
}