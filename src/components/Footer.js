import * as React from "react"
import "./footer.css"

export const Footer = (props) => {


    const page = props.page;
    const debaixo = props.subfooter;


    return (
        <footer>
            <h2>Aquí debería ir o footer modificado ({page})</h2>
            {debaixo && <p>{debaixo}</p>}
        </footer>
    )
}



