import * as React from "react";
import "./fontPreview.css";

export function FontPreview(props){

    return(
        <section className="font-preview-section">
           <h2>{props.name}</h2>
           <p>{props.description}</p>
        </section>
    )

}